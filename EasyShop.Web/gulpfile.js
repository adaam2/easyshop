﻿/// <binding ProjectOpened='sass:watch, js:watchNg' />
'use strict';

var gulp = require('gulp');
var sass = require('gulp-sass');
var watch = require('gulp-watch');
var uglify = require('gulp-uglify');
var ngAnnotate = require('gulp-ng-annotate');
var concat = require('gulp-concat');

gulp.task('sass:compile', function () {
    return gulp.src('./Styles/**/*.scss')
      .pipe(sass().on('error', sass.logError))
      .pipe(gulp.dest('./Content'));
});

gulp.task('sass:watch', function () {
    gulp.watch('./Styles/**/*.scss', ['sass:compile']);
});

gulp.task('js:watchNg', function () {
    gulp.watch('./Scripts/designer/**/*.js', ['js:minifyNg']);
});

gulp.task('js:minifyNg', function () {
    gulp.src(['./Scripts/designer/app.js', './Scripts/designer/*/*.js'])
      .pipe(concat('app.js'))
      .pipe(ngAnnotate())
      .pipe(uglify())
      .pipe(gulp.dest('./Scripts/min/'))
})