﻿'use strict';

app.factory('formService', ['$http', function ($http) {
    //comment
    var serviceBase = '/Designer/';
    var formServiceFactory = {};

    formServiceFactory.chooseTheme = function (themeId, callback) {
        $http({
            method: 'POST',
            url: serviceBase + 'ChooseTheme?id=' + themeId,
            withCredentials: false,
            headers: {
                'Content-Type': 'application/json'
            }
        })
        .success(function (data, status, headers, config) {
            // on success
            callback(data, status);
        })
        .error(function (data, status, headers, config) {
            // on error
            callback(data, status);
        });
    };

    formServiceFactory.pollPreviewWindow = function (callback) {

        //$http({
        //    method: 'GET',
        //    url: serviceBase + 'PollPreviewWindow',
        //    withCredentials: false,
        //    headers: {
        //        'Content-Type': 'application/json'
        //    }
        //})
        //.success(function (data, status, headers, config) {
        //    // on success
        //    callback(data, status);
        //})
        //.error(function (data, status, headers, config) {
        //    // on error
        //    callback(data, status);
        //});
    }

    return formServiceFactory;


}]);