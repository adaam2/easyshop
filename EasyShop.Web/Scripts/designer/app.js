﻿var app = angular.module('EasyShop.Designer', ['ui.router']);

var partialBase = '/Scripts/designer/partials/';

app.config(['$stateProvider', '$urlRouterProvider', function ($stateProvider, $urlRouterProvider) {
    $urlRouterProvider.otherwise('builder.profile');

    $stateProvider
        .state('builder', {
            abstract: true,
            templateUrl: partialBase + 'form.html',
            controller: 'formController'
        })
        .state('builder.profile', {
            url: '/profile',
            templateUrl: partialBase + 'form-profile.html'
        })
        .state('builder.design', {
            url: '/design',
            templateUrl: partialBase + 'form-design.html'
        })
        .state('builder.theme-picker', {
            url: '/theme-picker',
            templateUrl: partialBase + 'form-theme-picker.html'
        })
        .state('builder.finish', {
            url: '/complete',
            templateUrl: partialBase + 'form-end.html'
        })
}]);

app.run(['$rootScope', '$state', '$stateParams', function ($rootScope, $state, $stateParams) {
    $rootScope.$state = $state;
    $rootScope.$stateParams = $stateParams;
    $state.transitionTo('builder.profile');
}]);