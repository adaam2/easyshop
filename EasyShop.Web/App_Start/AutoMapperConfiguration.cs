﻿using AutoMapper;
using EasyShop.Models.Domain;
using EasyShop.Models.View;
using Ninject;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace EasyShop.Web.App_Start
{
    public static class AutoMapperConfiguration
    {
        public static MapperConfiguration Configure(IKernel kernel)
        {
            var config = new MapperConfiguration(cfg =>
            {
                cfg.ConstructServicesUsing(t => kernel.Get(t));

                // Map view models to models
                cfg.CreateMap<UserLoginViewModel, User>();
                cfg.CreateMap<UserRegistrationViewModel, User>();
            });

            return config;
        } 
    }
}