﻿using EasyShop.Models.Domain;
using EasyShop.Models.View;
using EasyShop.Services.Interfaces;
using Microsoft.AspNet.Identity;
using Microsoft.Owin.Security;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;

namespace EasyShop.Web.Controllers
{
    public class AuthController : Controller
    {
        private readonly UserManager<User> _userManager;

        public AuthController() : this(BaseController.UserManagerFactory.Invoke())
        {
            
        }

        public AuthController(UserManager<User> userManager)
        {
            this._userManager = userManager;
        }

        [AllowAnonymous]
        [HttpGet]
        // GET: Login
        public ActionResult Login(string returnUrl = "")
        {
            var model = new UserLoginViewModel()
            {
                ReturnUrl = returnUrl
            };
            return View(model);
        }

        [HttpPost]
        [AllowAnonymous]
        public async Task<ActionResult> Login(UserLoginViewModel model)
        {
            if (!ModelState.IsValid)
            {
                return View();
            }

            var user = await _userManager.FindAsync(model.Email, model.Password);

            if(user != null)
            {
                var identity = await _userManager.CreateIdentityAsync(user, DefaultAuthenticationTypes.ApplicationCookie);

                var authenticationManager = GetAuthManager();

                authenticationManager.SignIn(identity);

                return Redirect(GetRedirectUrl(model.ReturnUrl));
            }

            ModelState.AddModelError("", "Invalid email or password");
            return View();
        }

        [HttpGet]
        public ActionResult Logout()
        {
            var authManager = GetAuthManager();

            authManager.SignOut("ApplicationCookie");

            return RedirectToAction("Index", "Home", new { loggedOut = 1 });
        }

        [HttpGet]
        [AllowAnonymous]
        public ActionResult Register()
        {
            var model = new UserRegistrationViewModel()
            {
                
            };
            return View(model);
        }

        [HttpPost]
        [AllowAnonymous]
        public async Task<ActionResult> Register(UserRegistrationViewModel model)
        {
            if (!ModelState.IsValid)
            {
                return View();
            }

            var user = new User()
            {
                Email = model.Email,
                FirstName = model.FirstName,
                LastName = model.LastName,
                DateOfBirth = model.DateOfBirth,
                UserName = model.Email
            };

            var result = await _userManager.CreateAsync(user, model.Password);

            if (result.Succeeded)
            {
                var identity = await _userManager.CreateIdentityAsync(user, DefaultAuthenticationTypes.ApplicationCookie);

                return RedirectToAction("Index", "Account");
            }

            foreach(var error in result.Errors)
            {
                ModelState.AddModelError("", error);
            }

            return RedirectToAction("Login");
        }

        private string GetRedirectUrl(string returnUrl)
        {
            if (string.IsNullOrEmpty(returnUrl) || !Url.IsLocalUrl(returnUrl))
            {
                return Url.Action("Index", "Account");
            }

            return returnUrl;
        }

        private async Task SignIn(User user)
        {
            var identity = await _userManager.CreateIdentityAsync(user, DefaultAuthenticationTypes.ApplicationCookie);

            GetAuthManager().SignIn(identity);
        }

        private IAuthenticationManager GetAuthManager()
        {
            var ctx = Request.GetOwinContext();

            return ctx.Authentication;
        }
    }
}