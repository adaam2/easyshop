﻿using EasyShop.Common.Interfaces;
using EasyShop.Identity;
using EasyShop.Models.Domain;
using EasyShop.Web.App_Start;
using Microsoft.AspNet.Identity;
using Ninject;
using System;
using System.Web.Mvc;
using System.Web.Routing;

namespace EasyShop.Web.Controllers
{
    public abstract class BaseController : Controller
    {
        [Inject]
        public IUnitOfWork UnitOfWork { get; set; }

        public static Func<UserManager<User>> UserManagerFactory { get; private set; }

       
        protected override void Initialize(RequestContext requestContext)
        {
            ConfigureUserManagerFactory();

            base.Initialize(requestContext);
        }

        protected override void OnActionExecuting(ActionExecutingContext filterContext)
        {
            if (!filterContext.IsChildAction)
            {
                UnitOfWork.BeginTransaction();
            }              
        }

        protected override void OnResultExecuted(ResultExecutedContext filterContext)
        {
            if (!filterContext.IsChildAction)
            {
                UnitOfWork.Commit();
            }                
        }

        private void ConfigureUserManagerFactory()
        {
            UserManagerFactory = () =>
            {
                var userStore = new UserStore<User>(UnitOfWork);
                var userManager = new UserManager<User>(userStore);
                userManager.UserValidator = new UserValidator<User>(userManager)
                {
                    AllowOnlyAlphanumericUserNames = false,
                    RequireUniqueEmail = true
                };

                return userManager;
            };
        }
    }
}