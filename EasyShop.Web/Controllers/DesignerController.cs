﻿using EasyShop.Common.Attributes;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Threading.Tasks;

namespace EasyShop.Web.Controllers
{
    [AllowAnonymous]
    public class DesignerController : Controller
    {
        // GET: Designer
        public ActionResult Index()
        {
            return View();
        }

        [AjaxOnly]
        [HttpPost]
        public async Task<ActionResult> ChooseTheme(int id)
        {
            throw new NotImplementedException(); 
        }

        [HttpGet]
        //[AjaxOnly]
        public async Task<ActionResult> PollPreviewWindow()
        {
            return View("Preview");
        }
    }
}