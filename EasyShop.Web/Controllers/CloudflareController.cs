﻿using EasyShop.Cloudflare.DNS;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace EasyShop.Web.Controllers
{
    [AllowAnonymous]
    public class CloudflareController : Controller
    {
        // GET: Cloudflare
        public ActionResult Index()
        {
            var tester = new Tester();
            ViewBag.domains = tester.TesterA();
            
            return View();
        }
    }
}