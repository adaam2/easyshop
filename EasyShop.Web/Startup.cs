﻿using System;
using System.Threading.Tasks;
using Microsoft.Owin;
using Microsoft.Owin.Security.Cookies;
using Owin;
using Microsoft.AspNet.Identity;
using EasyShop.Models.Domain;
using EasyShop.Web.App_Start;
using EasyShop.Common.Interfaces;
using AutoMapper;

[assembly: OwinStartup(typeof(EasyShop.Web.Startup))]

namespace EasyShop.Web
{
    public class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            var cookieAuthenticationOptions = new CookieAuthenticationOptions()
            {
                AuthenticationType = "ApplicationCookie",
                LoginPath = new PathString("/auth/login")
            };

            app.UseCookieAuthentication(cookieAuthenticationOptions);
            
        }
    }
}
