﻿using FluentNHibernate.Cfg;
using NHibernate;
using NHibernate.Cfg;
using System;

namespace EasyShop.Common.Interfaces
{
    public interface IFluentInitializer
    {
        void BuildSchema(Configuration cfg);
        ISessionFactory GetFactory();
        FluentConfiguration BuildConfiguration();
    }
}
