﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EasyShop.Common.Interfaces
{
    public interface IEnvironmentService
    {
        bool IsDevelopment();

        string GetConnectionString();


    }
}
