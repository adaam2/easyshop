﻿using CloudFlare.API;
using CloudFlare.API.Data;
using System.Collections.Generic;
using System.Diagnostics;

namespace EasyShop.Cloudflare.DNS
{
    public class Tester
    {
        public List<ZoneObject> TesterA()
        {
            var domains = CFProxy.Access.GetDomains().objs;

            return domains;
        }
    }
}
