﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using EasyShop.Web.Controllers;
using EasyShop.Web.Tests.Utilities;
using Moq;
using Microsoft.AspNet.Identity;
using EasyShop.Models.Domain;
using EasyShop.Models.View;
using System.Web.Mvc;
using EasyShop.Identity;
using EasyShop.Data;
using System.Threading.Tasks;

namespace EasyShop.Web.Controllers.Tests
{
    [TestClass()]
    public class AuthControllerTests
    {
        private Mock<IUserStore<User>> _mockedUserStore;

        private UserManager<User> _userManager;

        private AuthController _controller;

        [TestInitialize]
        public void Initialize()
        {
            _mockedUserStore = new Mock<IUserStore<User>>();
            _userManager = new UserManager<User>(_mockedUserStore.Object);
            _controller = new AuthController(_userManager);
        }

        [TestMethod()]
        public void Login_Should_Redisplay_View_When_ModelState_Is_Invalid()
        {
            // Arrange
            UserLoginViewModel viewModel = new UserLoginViewModel()
            {
                Email = "",
                Password = "",
                ReturnUrl = ""
            };
            // Act
            ViewResult result = _controller.Login(viewModel).Result as ViewResult;

            // Assert that the view name is empty for this path

            Assert.AreEqual(string.Empty, result.ViewName);
        }

        [TestMethod()]
        public void Login_Should_Sign_In_And_Redirect_To_Redirect_Url()
        {
            // Arrange
            var user = new User()
            {
                UserName = "adam",
                Email = "adam@mbinteractive.co.uk",
                FirstName = "Adam",
                LastName = "Bull",
                PasswordHash = ""
            };
            _mockedUserStore.Setup(s => s.CreateAsync(user)).Returns(Task.FromResult(IdentityResult.Success));

            var userViewModel = new UserLoginViewModel()
            {
                Email = user.Email,
                Password = ""
            };
            // Act
            var result = _controller.Login(userViewModel).Result as ViewResult;

            // Assert

            Assert.AreEqual(string.Empty, result.ViewName);
        }
    }
}