﻿namespace EasyShop.Models.Domain
{
    public class Currency : Entity<string>
    {
        public virtual string Name { get; set; }
        public virtual string Symbol { get; set; }
        public virtual string Code { get; set; }
    }
}
