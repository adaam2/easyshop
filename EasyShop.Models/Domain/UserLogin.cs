﻿using EasyShop.Models.IdentityMisc;

namespace EasyShop.Models.Domain
{
    public class UserLogin : ValueObject
    {
        public virtual string LoginProvider { get; set; }

        public virtual string ProviderKey { get; set; }

    }
}
