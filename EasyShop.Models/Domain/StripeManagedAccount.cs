﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EasyShop.Models.Domain
{
    public class StripeManagedAccount : Entity<string>
    {
        public virtual string AccountId { get; set; }

        public virtual string CountryOfOrigin { get; set; }

        protected virtual string SecretKey { get; set; }

        protected virtual string PublishableKey { get; set; }
    }
}
