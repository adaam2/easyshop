﻿using System;
using FluentNHibernate.Automapping;
using FluentNHibernate.Automapping.Alterations;
using NHibernate.Mapping.ByCode;
using NHibernate.Mapping.ByCode.Conformist;

namespace EasyShop.Models.Domain
{
    public class UserClaim : Entity<string>
    {
        public virtual string ClaimType { get; set; }

        public virtual string ClaimValue { get; set; }

        public virtual User User { get; set; }
    }

    public class UserClaimMap : IAutoMappingOverride<UserClaim>
    {
        public void Override(AutoMapping<UserClaim> mapping)
        {
            mapping.Table("AspNetUserClaims");
            mapping.Id(x => x.Id).GeneratedBy.UuidHex("D");
            mapping.Map(x => x.ClaimType);
            mapping.Map(x => x.ClaimValue);

            mapping.HasOne(x => x.User).ForeignKey("UserId");
        }
    }
}
