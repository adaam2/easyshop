﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EasyShop.Models.Domain
{
    public class SubscriptionPlan : Entity<string>
    {
        public virtual string Name { get; set; }

        public virtual bool IsPaidSubscription { get; set; }
    }
}
