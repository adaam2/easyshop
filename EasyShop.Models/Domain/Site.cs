﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EasyShop.Models.Domain
{
    public class Site : Entity<string>
    {
        public virtual string Name { get; set; }

        public virtual User SiteAdministrator { get; set; }

        public virtual Currency SiteCurrency { get; set; }

        protected virtual DateTime CreatedAt { get; set; }

        protected virtual bool ForceSSL { get; set; }

        public virtual string Domain { get; set; }

        public virtual string SiteLanguageIso { get; set; }

        public virtual string TwitterHandle { get; set; }

        public virtual string FacebookUrl { get; set; }

        public virtual string LogoUrl { get; set; }

        public virtual string BackgroundImage { get; set; }

        public virtual bool BackgroundImageIsTiled { get; set; }

        public virtual string BodyBackgroundColour { get; set; }

        public virtual string BodyTextColour { get; set; }

        public virtual string AccentColour { get; set; }

        public virtual string ButtonBackgroundColour { get; set; }

        public virtual string ButtonForegroundColour { get; set; }

        public virtual string HeadingSizeInPixels { get; set; }

        public virtual StripeManagedAccount StripeManagedAccount { get; set; }
    }
}
