﻿using FluentNHibernate.Automapping.Alterations;
using Microsoft.AspNet.Identity;
using NHibernate.Mapping.ByCode;
using NHibernate.Mapping.ByCode.Conformist;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Security.Claims;
using System.Text;
using System.Threading.Tasks;
using FluentNHibernate.Automapping;

namespace EasyShop.Models.Domain
{
    public class User : Entity<string>, IUser
    {
        public virtual int AccessFailedCount { get; set; }

        public virtual string Email { get; set; }

        public virtual bool EmailConfirmed { get; set; }

        public virtual bool LockoutEnabled { get; set; }

        public virtual DateTime? LockoutEndDateUtc { get; set; }

        public virtual string PasswordHash { get; set; }

        public virtual string PhoneNumber { get; set; }

        public virtual bool PhoneNumberConfirmed { get; set; }

        public virtual bool TwoFactorEnabled { get; set; }

        public virtual string UserName { get; set; }

        public virtual string SecurityStamp { get; set; }

        public virtual string FirstName { get; set; }

        public virtual string LastName { get; set; }

        public virtual DateTime? DateOfBirth { get; set; }

        public virtual ICollection<UserRole> Roles { get; protected set; }

        public virtual ICollection<UserClaim> Claims { get; protected set; }

        public virtual ICollection<UserLogin> Logins { get; protected set; }

        public User()
        {
            this.Roles = new List<UserRole>();
            this.Claims = new List<UserClaim>();
            this.Logins = new List<UserLogin>();
        }

        public User(string userName)
            : this()
        {
            this.UserName = userName;
        }
    }

    public class UserMap : IAutoMappingOverride<User>
    {
        public void Override(AutoMapping<User> mapping)
        {
            mapping.Table("AspNetUsers");
            mapping.Id(x => x.Id).GeneratedBy.UuidHex("D");

            mapping.Map(x => x.AccessFailedCount);

            mapping.Map(x => x.Email);

            mapping.Map(x => x.EmailConfirmed);

            mapping.Map(x => x.LockoutEnabled);

            mapping.Map(x => x.LockoutEndDateUtc);

            mapping.Map(x => x.PasswordHash);

            mapping.Map(x => x.PhoneNumber);

            mapping.Map(x => x.PhoneNumberConfirmed);

            mapping.Map(x => x.TwoFactorEnabled);

            mapping.Map(x => x.FirstName);

            mapping.Map(x => x.LastName);

            mapping.Map(x => x.DateOfBirth).Nullable();

            mapping.Map(x => x.UserName).Length(255).Nullable().Unique();

            mapping.Map(x => x.SecurityStamp);

            mapping.HasMany(x => x.Claims).AsBag().KeyColumn("UserId").Cascade.AllDeleteOrphan().Not.KeyUpdate();

            mapping.HasMany(x => x.Logins)
                .Table("AspNetUserLogins")
                .KeyColumn("UserId")
                .Component(c => {
                    c.Map(p => p.LoginProvider);
                    c.Map(p => p.ProviderKey);
            });


            mapping.HasManyToMany(x => x.Roles)
                .AsBag()
                .Table("AspNetUserRoles")
                .Inverse()
                .ParentKeyColumn("UserId")
                .ChildKeyColumn("RoleId");
               
        }
    }
}
