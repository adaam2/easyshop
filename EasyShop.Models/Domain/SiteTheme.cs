﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EasyShop.Models.Domain
{
    public class SiteTheme : Entity<string>
    {
        public virtual string Name { get; set; }
        public virtual string GitRepositoryUrl { get; set; }
        public virtual string PreviewServerUrl { get; set; }
        public virtual IList<Site> SitesUsingTheme { get; set; }
    }
}
