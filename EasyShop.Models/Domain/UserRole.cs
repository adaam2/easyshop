﻿using FluentNHibernate.Automapping.Alterations;
using Microsoft.AspNet.Identity;
using NHibernate.Mapping.ByCode;
using NHibernate.Mapping.ByCode.Conformist;
using System.Collections.Generic;
using FluentNHibernate.Automapping;
using System;

namespace EasyShop.Models.Domain
{
    public class UserRole : Entity<string>, IRole
    {
        public virtual string Name { get; set; }

        public virtual ICollection<User> Users { get; protected set; }

        public UserRole()
        {
            this.Users = (ICollection<User>)new List<User>();
        }

        public UserRole(string roleName) : this()
        {
            this.Name = roleName;
        }
    }

    public class UserRoleMap : IAutoMappingOverride<UserRole>
    {

        public void Override(AutoMapping<UserRole> mapping)
        {
            mapping.Table("AspNetRoles");
            mapping.Id(x => x.Id).GeneratedBy.UuidHex("D");
            mapping.Map(x => x.Name).Length(255).Unique().Not.Nullable();
            mapping.HasManyToMany(x => x.Users)
                .AsBag()
                .Table("AspNetUserRoles")
                .ParentKeyColumn("RoleId")
                .ChildKeyColumn("UserId")
                .Cascade.None();
                
            //    , map =>
            //{
            //    map.Table("AspNetUserRoles");
            //    map.Cascade(Cascade.None);
            //    map.Key(k => k.Column("RoleId"));
            //}, rel => rel.ManyToMany(p => p.Column("UserId")));
        }
    }
}
