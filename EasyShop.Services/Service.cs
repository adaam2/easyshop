﻿using EasyShop.Data.Interfaces;
using EasyShop.Models.Domain;
using EasyShop.Services.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;

namespace EasyShop.Services
{
    public class Service<T> : IService<T> where T : Entity<string>
    {
        private readonly IRepository<T> _repository;

        public Service(IRepository<T> repository)
        {
            _repository = repository;
        }

        public IList<T> All()
        {
            return _repository.All().ToList();
        }

        public string Create(T obj)
        {
            return _repository.Create(obj);
        }

        public void Delete(string id)
        {
            _repository.Delete(id);
        }

        public IList<T> FilterBy(Expression<Func<T, bool>> expression)
        {
            return _repository.FilterBy(expression).ToList();
        }

        public T Find(string id)
        {
            return _repository.FindBy(o => o.Id == id);
        }

        public T FindBy(Expression<Func<T, bool>> expression)
        {
            return _repository.FindBy(expression);
        }

        public IQueryable<T> GetQueryable()
        {
            return _repository.All();
        }

        public void Update(T obj)
        {
            _repository.Update(obj);
        }
    }
}
