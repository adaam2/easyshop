﻿using Stripe;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EasyShop.Services.Interfaces
{
    public interface IStripeService
    {
        StripeCharge ProcessCharge(decimal amount, string cardToken);
        StripeCustomer CreateCustomer(string email, string name, string cardToken = "");
    }
}
