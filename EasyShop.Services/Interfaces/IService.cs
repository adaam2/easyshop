﻿using EasyShop.Models.Domain;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;

namespace EasyShop.Services.Interfaces
{
    public interface IService<T> where T : Entity<string>
    {
        T Find(string id);
        T FindBy(Expression<Func<T, bool>> expression);
        IList<T> FilterBy(Expression<Func<T, bool>> expression);
        IList<T> All();
        string Create(T obj);
        void Update(T obj);
        void Delete(string id);
        IQueryable<T> GetQueryable();
    }
}
