﻿using EasyShop.Models.Domain;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;

namespace EasyShop.Data.Interfaces
{
    public interface IRepository<T> where T : Entity<string>
    {
        IQueryable<T> All();
        T FindBy(Expression<Func<T, bool>> expression);
        IQueryable<T> FilterBy(Expression<Func<T, bool>> expression);
        string Create(T entity);
        void Update(T entity);
        void Delete(string id);
    }
}
