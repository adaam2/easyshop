﻿using NHibernate.Event;
using NHibernate.Persister.Entity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Principal;
using System.Text;
using System.Threading.Tasks;

namespace EasyShop.Data.NHibernate.EventListeners
{
    public class AutoGenerateDateOnRecordInsert : IPreInsertEventListener
    {
        public bool OnPreInsert(PreInsertEvent @event)
        {
            
            var time = DateTime.Now;

            Set(@event.Persister, @event.State, "Created", time);

            return false;
        }

        private void Set(IEntityPersister persister, object[] state, string propertyName, object value)
        {
            var index = Array.IndexOf(persister.PropertyNames, propertyName);
            if (index == -1)
                return;
            state[index] = value;
        }
    }
}
