﻿using EasyShop.Common.Interfaces;
using NHibernate;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EasyShop.Data
{
    public class UnitOfWork : IUnitOfWork
    {
        private readonly ISessionFactory _sessionFactory;

        private ITransaction _transaction;

        public ISession Session { get; private set; }

        /// <summary>
        /// The session factory interface => concrete implementation is bound in the ninjectwebcommon file.
        /// </summary>
        /// <param name="sessionFactory"></param>
        public UnitOfWork(ISessionFactory sessionFactory)
        {
            _sessionFactory = sessionFactory;

            Session = _sessionFactory.OpenSession();
        }

        public void BeginTransaction()
        {
            _transaction = Session.BeginTransaction();
        }

        public void Commit()
        {
            if(_transaction != null && _transaction.IsActive)
            {
                _transaction.Commit();
            }
        }

        public void Rollback()
        {
            if (_transaction != null && _transaction.IsActive)
            {
                _transaction.Rollback();
            }
        }

        public void Dispose()
        {
            if (Session.IsOpen)
            {
                Session.Dispose();
            }
        }
    }
}
