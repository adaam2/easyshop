﻿using EasyShop.Common.Interfaces;
using EasyShop.Data.Interfaces;
using EasyShop.Models.Domain;
using NHibernate;
using NHibernate.Linq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;

namespace EasyShop.Data
{
    public class Repository<T> : IRepository<T> where T : Entity<string>
    {
        private UnitOfWork _unitOfWork;

        public Repository(IUnitOfWork unitOfWork)
        {
            _unitOfWork = (UnitOfWork)unitOfWork;
        }

        protected ISession Session { get { return _unitOfWork.Session; } }

        public T GetById(int id)
        {
            return Session.Get<T>(id);
        }

        public string Create(T entity)
        {
            Session.Save(entity);

            return entity.Id;
        }

        public void Update(T entity)
        {
            Session.Update(entity);
        }

        public void Delete(string id)
        {
            Session.Delete(Session.Load<T>(id));
        }

        public IQueryable<T> All()
        {
            return Session.Query<T>();

        }

        public T FindBy(Expression<Func<T, bool>> expression)
        {
            return All().Where(expression).FirstOrDefault();
        }

        public IQueryable<T> FilterBy(Expression<Func<T, bool>> expression)
        {
            return All().Where(expression);
        }
    }
}
